# /etc/nginx/nginx.conf

user root;

# Set number of worker processes automatically based on number of CPU cores.
worker_processes 1;

# Enables the use of JIT for regular expressions to speed-up their processing.
pcre_jit on;

events {
        # The maximum number of simultaneous connections that can be opened by
        # a worker process.
        worker_connections 1024;
}

http {
    # Includes mapping of file name extensions to MIME types of responses
    # and defines the default type.
    include /etc/nginx/mime.types;
    default_type application/octet-stream;
    server_tokens off;

    client_max_body_size 2400m;
    keepalive_timeout  24000;

    log_format  main  escape=json '{'
        '"request":"$request",'
        '"status": "$status",'
        '"request_time":"$request_time",'
        '"http_user_agent":"$http_user_agent"'
    '}';

    log_format  error  escape=json '{'
        '"request":"$request",'
        '"status": "$status",'
        '"request_time":"$request_time",'
        '"http_user_agent":"$http_user_agent"'
    '}';

    # reduce the data that needs to be sent over network -- for testing environment
    gzip on;
    # gzip_static on;
    gzip_min_length 512;
    gzip_comp_level 1;
    gzip_vary on;
    gzip_disable msie6;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types
        text/css
        text/javascript
        text/xml
        text/plain
        text/x-component
        application/javascript
        application/x-javascript
        application/json
        application/xml
        application/rss+xml
        application/atom+xml
        font/truetype
        font/opentype
        application/vnd.ms-fontobject
        image/svg+xml;

    server {
        listen 80;

        root /var/www/public;
        index index.php index.html;

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload";
        add_header X-Frame-Options "SAMEORIGIN";
        add_header X-Content-Type-Options "nosniff" always;
        add_header X-Xss-Protection "1; mode=block" always;

        server_name _;

        location / {

            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE, HEAD';
            add_header 'Access-Control-Allow-Headers' 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,some_my_tokens';

            try_files $uri $uri/ /index.php?$query_string;

        }

        location ~ \.php$ {
            try_files $uri /index.php =404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param SCRIPT_NAME $fastcgi_script_name;
            fastcgi_index index.php;
            fastcgi_buffers 16 16k;
            fastcgi_buffer_size 32k;
            fastcgi_read_timeout 50000;
            include fastcgi_params;
        }

        location ~* \.(eot|otf|ttf|woff|woff2)$ {
            add_header Access-Control-Allow-Origin *;
        }

        location ~ /\. {
            deny all;
        }

        error_log /dev/stderr error;
        access_log /dev/stdout main;
    }
}
